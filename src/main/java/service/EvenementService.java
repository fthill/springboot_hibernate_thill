/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.List;
import metier.Evenement;
import java.util.Objects;


import org.hibernate.*;
import org.springframework.stereotype.Service;

/**
 *
 * @author formation
 */
@Service
public class EvenementService {
    private final SessionFactory sessionFactory;
    
    public EvenementService(SessionFactory sessionFactory){
            this.sessionFactory = Objects.requireNonNull(sessionFactory);
    }
    
    public void insertEvenement(Evenement evenement){
        Objects.requireNonNull(evenement);
        Session session = this.sessionFactory.openSession();
        session.beginTransaction();
        session.save(evenement);
        session.getTransaction().commit();
        session.close();
    }
    
    public void deleteEvenement(Evenement evenement){
        Objects.requireNonNull(evenement);
        Session session = this.sessionFactory.openSession();
        session.beginTransaction();
        session.delete(evenement);
        session.getTransaction().commit();
        session.close();
    }
    
    public void modifyEvenement(Evenement evenement){
        Objects.requireNonNull(evenement);
        Session session = this.sessionFactory.openSession();
        session.beginTransaction();
        session.update(evenement);
        session.getTransaction().commit();
        session.close();
    }
    
    
    
    @SuppressWarnings("unchecked")
    public List<Evenement> getAllEvenements() {
        Session session = this.sessionFactory.openSession();
        List<Evenement> result = session.createQuery("from Evenement").list();
        session.close();
        return result;
    }
}
