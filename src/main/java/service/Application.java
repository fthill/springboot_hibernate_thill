package service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
    	// Allows large request headers 
    	System.setProperty("server.tomcat.maxHttpHeaderSize","10000000");
        SpringApplication.run(Application.class, args);
    }
}
