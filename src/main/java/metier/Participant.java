/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier;
 
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author formation
 */
@Entity
@Table(name = "participant", uniqueConstraints={@UniqueConstraint(columnNames={"num_pers"})})
public class Participant {
    
    public Participant(String nom, String prenom, String email, String organisation, String observations) {
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.organisation = organisation;
        this.observations = observations;
    }
 
    @Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy="increment")
    private int num_pers;
 
    @Column(name = "nom", length=20, nullable=true)
    private String nom;
 
    @Column(name="prenom", length=20, nullable=true)
    private String prenom;
    
    @Column(name="email", length=20, nullable=true)
    private String email;
    
    @Column(name="organisation", length=100, nullable=true)
    private String organisation;
    
    @Column(name="observations", length=100, nullable=true)
    private String observations;
    
    @ManyToOne
    @JoinColumn(name="event", nullable=true)
    private Evenement event;
    
    public int getId(){
        return num_pers;
    }
    
    public Evenement getEvenement() {
        return this.event;
    }    
}
