/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author formation
 */
@Entity
@Table(name = "evenement", uniqueConstraints={@UniqueConstraint(columnNames={"num_even"})})
public class Evenement {
    
    public Evenement(String intitule, String theme, String date_duree, String duree, int nb_part_max, List<Participant> participants) {
        this.intitule = intitule;
        this.theme = theme;
        this.date_duree = date_duree;
        this.duree = duree;
        this.nb_part_max = nb_part_max;
        this.participants = participants;

    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "num_even", length=11, nullable=false, unique=true)
    private Integer num_even;
 
    @Column(name = "intitule", length=20, nullable=true)
    private String intitule;
 
    @Column(name="theme", length=20, nullable=true)
    private String theme;
    
    @Column(name="date_duree", length=20, nullable=true)
    private String date_duree;
    
    @Column(name="duree", length=100, nullable=true)
    private String duree;
    
    @Column(name="nb_part_max", length=100, nullable=true)
    private int nb_part_max;
    
    @OneToMany(mappedBy ="event")
    private List<Participant> participants = new ArrayList<>();
    
    public List<Participant> getParticipants(){
        return participants;
    }
    
    public void addParticipant(Participant participant) {
        if (participant.getEvenement().equals(null))
            this.participants.add(participant);
    }
}
