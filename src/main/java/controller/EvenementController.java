package controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import metier.Participant;
import service.EvenementService;

@Controller
public class EvenementController {
	
	@Autowired
	private final EvenementService evenementService;
	
	@RequestMapping(value = "/listParticipants", method = RequestMethod.GET)
    public ResponseEntity<List<Participant>> getAllParticipants() {
		List<Participant> participants = evenementService.getAllEvenements();
		return new ResponseEntity<>(participants, HttpStatus.OK);
    } 
}
