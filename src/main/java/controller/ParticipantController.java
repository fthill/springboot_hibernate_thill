/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import metier.Participant;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import service.ParticipantService;

@Controller
public class ParticipantController {
	
	@Autowired
	private ParticipantService participantService;
	
    @RequestMapping(value = "/listParticipants", method = RequestMethod.GET)
    public ResponseEntity<List<Participant>> getAllParticipants() {
		List<Participant> participants = participantService.getAllParticipants();
		return new ResponseEntity<>(participants, HttpStatus.OK);
    }  
    	
}

