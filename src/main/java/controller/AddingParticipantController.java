/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.List;
import metier.Participant;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import service.ParticipantService;

/**
 *
 * @author formation
 */
public class AddingParticipantController {
    private static SessionFactory createSessionFactory() {
        final StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
        try{
            return new MetadataSources(registry).buildMetadata().buildSessionFactory();
        } catch (Exception e){
            e.printStackTrace();
            StandardServiceRegistryBuilder.destroy(registry);
        }
        return null;
    }
    @Autowired
	private ParticipantService participantService;
	
        @GetMapping(value = "/addParticipant")
	public ResponseEntity<String> getAllParticipants(@RequestParam(value="nom") String nom, @RequestParam(value="prenom") String prenom, @RequestParam(value="email") String email, @RequestParam(value="organisation") String organisation, @RequestParam(value="observation") String observation) {
		Participant participant = new Participant(nom, prenom, email, organisation, observation);
                participantService.insertParticipant(participant);
                return new ResponseEntity<>("Le participant a bien été inséré dans la base", HttpStatus.OK);
        }        
}
